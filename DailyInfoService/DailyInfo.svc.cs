﻿using DatabaseLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DailyInfoService
{
    public class DailyInfo : IDailyInfo
    {
        public IEnumerable<CurrencyRate> GetCurrencyRatesOnToday()
        {
            return GetCurrencyRatesOnDate(DateTime.Today);
        }

        public CurrencyRate GetCurrencyRateOnTodayByCode(int code)
        {
            return GetCurrencyRateOnDateByCode(DateTime.Today, code);
        }
            

        public IEnumerable<CurrencyRate> GetCurrencyRatesOnDate(DateTime date)
        {
            var db = new DatabaseContext();
            return db.CurrencyRatesOnDate
                .Where(vcod => vcod.Date == date.Date)
                .Select(vcod => new CurrencyRate
                {
                    Code = vcod.Code,
                    ChCode = vcod.ChCode,
                    Name = vcod.Name,
                    Denomination = vcod.Denomination,
                    Rate = vcod.Rate
                })
                .ToList();
        }

        public CurrencyRate GetCurrencyRateOnDateByCode(DateTime date, int code)
        {
            var db = new DatabaseContext();
            return db.CurrencyRatesOnDate
                .Where(vcod => vcod.Date == date.Date
                    && vcod.Code == code)
                .Select(vcod => new CurrencyRate
                {
                    Code = vcod.Code,
                    ChCode = vcod.ChCode,
                    Name = vcod.Name,
                    Denomination = vcod.Denomination,
                    Rate = vcod.Rate
                })
                .FirstOrDefault();
        }
    }
}
