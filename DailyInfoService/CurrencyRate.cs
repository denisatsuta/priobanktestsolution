﻿using System.Runtime.Serialization;

namespace DailyInfoService
{
    [DataContract]
    public class CurrencyRate
    {
        /// <summary>
        /// Название валюты
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Номинал
        /// </summary>
        [DataMember]
        public decimal Denomination { get; set; }

        /// <summary>
        /// Курс
        /// </summary>
        [DataMember]
        public decimal Rate { get; set; }

        /// <summary>
        ///  ISO Цифровой код валюты
        /// </summary>
        [DataMember]
        public int Code { get; set; }

        /// <summary>
        /// ISO Символьный код валюты
        /// </summary>
        [DataMember]
        public string ChCode { get; set; }
    }
}
