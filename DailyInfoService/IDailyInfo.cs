﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DailyInfoService
{
    [ServiceContract]
    public interface IDailyInfo
    {
        [OperationContract]
        IEnumerable<CurrencyRate> GetCurrencyRatesOnToday();

        [OperationContract]
        CurrencyRate GetCurrencyRateOnTodayByCode(int code);

        [OperationContract]
        IEnumerable<CurrencyRate> GetCurrencyRatesOnDate(DateTime date);

        [OperationContract]
        CurrencyRate GetCurrencyRateOnDateByCode(DateTime date, int code);
    }
}
