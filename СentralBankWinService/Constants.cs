﻿using Quartz;

namespace СentralBankWinService
{
    static class Constants
    {
        public const string SyncCurrencyRatesJobName = "SyncCurrencyRatesJob";
        public const string SyncCurrencyRatesJobGroup = "SyncCurrencyRatesJobGroup";
        public const string SyncCurrencyRatesTriggerName = "SyncCurrencyRatesTrigger";
        public const string SyncCurrencyRatesTriggerGroup = "SyncCurrencyRatesTriggerGroup";
        public const string JobFailureListenerName = "FailJobListener";
        public const string FailureTriggerName = "FailureTrigger";
        public const string FailureTriggerGroup = "FailureTriggerGroup";

        public static readonly JobKey SyncCurrencyRatesJobKey = new JobKey(SyncCurrencyRatesJobName, SyncCurrencyRatesJobGroup);
        public static readonly TriggerKey SyncCurrencyRatesTriggerKey = new TriggerKey(SyncCurrencyRatesTriggerName, SyncCurrencyRatesTriggerGroup);
        public static readonly TriggerKey FailureTriggerKey = new TriggerKey(FailureTriggerName, FailureTriggerGroup);
        
    }
}
