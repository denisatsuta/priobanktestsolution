﻿using DatabaseLibrary;
using Quartz;
using System;
using СentralBankServices.CBServices;
using СentralBankServices;

namespace СentralBankWinService
{
    [DisallowConcurrentExecution]
    class SyncCurrencyRatesJob : IJob
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var date = DateTime.Today;
                using (var db = new DatabaseContext())
                {
                    var client = new DailyInfoSoapClient();
                    var service = new SyncCurrencyRates(client, db);

                    service.SyncCurrencyRatesOnDate(date);

                    client.Close();
                }
                log.Info($"Синхронизация за {date:dd.MM.yyyy} успешно завершена.");
            }
            catch (Exception e)
            {
                log.Error(e);
                throw;
            }
        }
    }
}
